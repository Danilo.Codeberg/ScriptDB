-- creo tabella --
CREATE TABLE articoli (
	codicearticolo serial PRIMARY KEY,
	nomearticolo VARCHAR ( 50 ) UNIQUE NOT NULL,
	descrizionearticolo varchar NULL,
	prezzoarticolo float4 NULL,
	CONSTRAINT articoli_pk PRIMARY KEY (codicearticolo)
);
-- droppo tabella
DROP TABLE public.articoli;
-- ricreo la tabella
CREATE TABLE public.articoli (
	codicearticolo int4 NOT NULL,
	nomearticolo varchar NULL,
	descrizionearticolo varchar NULL,
	prezzoarticolo float4 NULL,
	CONSTRAINT articoli_pk PRIMARY KEY (codicearticolo)
);
-- inserisco robe
INSERT INTO public.articoli(codicearticolo, nomearticolo, descrizionearticolo, prezzoarticolo)
VALUES 
(1, 'sturalavandino', 'attrezzo molto utile', 2),
(2, 'scarpe brutte', 'molto brutte', 40),
(3, 'coltello', 'sempre utile', 6),
(4, 'coso strano', 'bo!', 500);
-- update de cose
UPDATE public.articoli
SET prezzoarticolo = '1000' 
WHERE codicearticolo = 3;
-- elimino cose
DELETE FROM public.articoli
WHERE codicearticolo = 1;