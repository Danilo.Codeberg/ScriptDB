-- creo tabella --
CREATE TABLE articoli (
	codicearticolo serial PRIMARY KEY,
	nomearticolo VARCHAR ( 50 ) UNIQUE NOT NULL,
	descrizionearticolo varchar NULL,
	prezzoarticolo float4 NULL,
	CONSTRAINT articoli_pk PRIMARY KEY (codicearticolo)
);